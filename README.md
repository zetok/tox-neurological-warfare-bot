This is a spambot for Tox. It's the best `;)`

More features may come along the way `:)`

Any contributions are welcome.

Tox ID of a running bot: `C7719C6808C14B77348004956D1D98046CE09A34370E7608150EAD74C3815D30C8BA3AB9BEB9`

And QR code:

[![QR code](/img/spambot-id.png)](tox:C7719C6808C14B77348004956D1D98046CE09A34370E7608150EAD74C3815D30C8BA3AB9BEB9)

Build status: [![Build Status](https://travis-ci.org/zetok/tox-rust-neurological-warfare-bot.svg)](https://travis-ci.org/zetok/tox-rust-neurological-warfare-bot)


# Features

* spam for 1-on-1 chats
* saving & loading Tox data file


# Installation
Installation is fairly simple. This bot will work only on Linux.

Newest [**toxcore**](https://github.com/irungentoo/toxcore) is required. For instructions on how to get it, please refer to its [INSTALL.md](https://github.com/irungentoo/toxcore/blob/master/INSTALL.md).

1. Install [Rust](http://www.rust-lang.org/)
2. Make with `cargo build`
3. Run with `./target/debug/./tox-rust-neurological-warfare-bot`

# Usage

Launch and message it to receive spam.

To receive constant stream of spam, use command to trigger it.


Currently supported by bot commands are:

## Friend commands

| Command | What it does |
|---------|--------------|
| id | Messages its ID |
| start | Start continously sending spam |
| stop | Stop continous spam |
| spam 10 1000ms | Send 10 spam messages, with 1000ms intervals between messages. |

Sending `start` multiple times increases speed at which bot sends spam.


# License

Licensed under GPLv3+, for details see [COPYING](/COPYING).