/*
    Copyright © 2015 Zetok Zalbavar <zetok@openmailbox.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



/*
    Binding to toxcore
*/
extern crate rstox;
use rstox::core::*;


/*
    For various stuff
*/
extern crate rand;
use rand::ThreadRng;
use rand::Rng;
use std::thread;
use std::sync::mpsc::channel;
use std::sync::mpsc::Receiver;


/*
    For loading and writing Tox data
*/
extern crate chrono;
use chrono::UTC;

mod for_files;


/*
    Bot's own stuff
*/
// TODO: when other functions will be moved from main.rs, things should be
//       added here
mod bootstrap;



#[derive(Debug,Clone)]
struct Spam {
    iters: u32,
    wait:  u32,
}



struct Bot {
    /**
        Cached RNG, apparently it helps with RNG's performance when it's used
        a lot.
    */
    random: ThreadRng,

    /**
        List of peers to which spam should be sent
    */
    spam: Vec<u32>,

    /**
        Vector with customizable version of spam.
    */
    cust_spam: Vec<(u32, Spam)>,

    /**
        Time since last save.
    */
    last_save: i64,

    receivers: Vec<Receiver<u32>>,
}


impl Bot {
    //fn new(data: Option<Vec<u8>>) -> Self {
    fn new() -> Self {
        Bot {
            //tox: Tox::new(ToxOptions::new(), data.as_ref()
            //                                .map(|x| &**x)).unwrap(),
            random: rand::thread_rng(),
            spam: vec![],
            cust_spam: vec![],
            last_save: UTC::now().timestamp(),
            receivers: vec![],
        }
    }
}


/**
    Put a message to stdout, prepended with current timestamp.
*/
pub fn log(s: &str) {
    println!("{}: {}", UTC::now(), s);
}


/**
    Get some random data as UTF-8 string of `len` length and a source of
    randomness.
*/
fn rand_string(len: usize, rng: &mut ThreadRng) -> String {
    let mut vec: Vec<u8> = Vec::with_capacity(len);

    for _ in 0..len {
        vec.push(rng.gen::<u8>());
    }

    unsafe { String::from_utf8_unchecked(vec) }
}


/**
    Send a message of random length to `fnum` friend.
*/
fn spam_message(tox: &mut Tox, fnum: u32, random: &mut ThreadRng) {
    let reply_msg: String = rand_string((1372.0 * random.gen::<f64>()) as usize, random);
    drop(tox.send_friend_message(fnum, MessageType::Normal, &reply_msg));
}


/**
    In its own thread crouches spam, waiting for a chance to strike.
*/
fn hidden_iter_crouching_spam(fnum: u32, spam: Spam) -> Receiver<u32> {
    let (tx, rx) = channel::<u32>();
    let tx = tx.clone();
    thread::spawn(move || {
        for _crouch in 0..spam.iters {
            thread::sleep_ms(spam.wait);
            tx.send(fnum).unwrap();
        }
    });
    rx
}

/*
    Function to deal with incoming friend requests, all are accepted.
*/
fn on_friend_request(tox: &mut Tox, fpk: PublicKey, msg: String) {
    drop(tox.add_friend_norequest(&fpk));
    log(&format!("Friend {} with friend message {:?} was added.", fpk, msg));
}

/*
    Function to deal with friend messages.

    Every message should be replied with a random message.

    Message can trigger constant stream of random messages, turn it off,
    or send an ID to friend.

*/
fn on_friend_message(tox: &mut Tox, fnum: u32, msg: String, bot: &mut Bot) {

    match &*msg {
        ".id" | "id" | "ID" => {
            let message = format!("My ID: {}", tox.get_address());
            drop(tox.send_friend_message(fnum, MessageType::Normal, &message));
        },

        /*
            `start` should append friend to spamlist, regardless of whether
            they are already on it.

            Adding multiple entries of friend increases speed at which spam
            is being sent to friend.
        */
        "start" => {
            bot.spam.push(fnum);
            log(&format!("Friend {} added to spam list.", fnum));
            log(&format!("Spam list has {} friend(s): {:?}", bot.spam.len(),
                                                             bot.spam));
        },

        /*
            `stop` should remove all entries of a given friend from spamlist
        */
        "stop" => {
            friend_remove_spam(bot, fnum);
        },

        m => {
            if let Some(spam) = parse_message(m) {
                bot.cust_spam.push((fnum, spam));
            } else {
                spam_message(tox, fnum, &mut bot.random);
            }
        },
    }
}


/**
    Function to deal with removing friend from spamlist.

    Removes all entries of friend from spamlist.
*/
fn friend_remove_spam(bot: &mut Bot, fnum: u32) {
    let mut removed: bool = false;
    // loop is needed to ensure that list is fully iterated through
    while !removed {
        // end loop if condition isn't met
        removed = true;
        for f in 0..bot.spam.len() {
            if bot.spam[f] == fnum {
                // loop again if condition has been met
                removed = false;

                // actual work
                drop(bot.spam.remove(f));
                log(&format!("Friend {} removed from spam list.",
                             fnum));
                log(&format!("Spam list has {} friend(s): {:?}",
                             bot.spam.len(),
                             bot.spam));

                // break iterator after removing part of it, without
                // that bug of `index out of bounds` would be
                // triggered (due to relying on `len()` of old vector)
                break;
            }
        }
    }
}


fn parse_message(msg: &str) -> Option<Spam> {
    let msg = msg.to_string();
    let args: Vec<&str> = msg.split_whitespace().collect();

    if args.len() == 3 && args[0] == "spam" {
        if let Some(iterations) = args[1].parse::<u32>().ok() {
            if let Some(wait) = args[2].trim_right_matches("ms").parse::<u32>().ok() {
                return Some(Spam { iters: iterations, wait: wait })
            }
        }
    }
    None
}


fn main() {
    /*
        Try to load data file, if not possible, print an error and generate
        new Tox instance.
    */
    let data = for_files::load_save("bot.tox")
                .map_err(|e| println!("\n{}: Error loading save: {}\n",
                                      UTC::now(), e))
                .ok();
    let mut tox = Tox::new(ToxOptions::new(), data.as_ref()
                                            .map(|x| &**x)).unwrap();


    drop(tox.set_name("THIS. IS. TOX!\0"));
    drop(tox.set_status_message("Send \"start\" to start spam and \"stop\" to stop it."));

    /*
        Bot stuff
    */
    let mut bot = Bot::new();



    /*
        Boostrapping process
        During bootstrapping one should query random bootstrap nodes from a
        supplied list; in case where there is no list, rely back on hardcoded
        bootstrap nodes.
        // TODO: actually make it possible to use supplied list; location of a
        //       list should be determined by value supplied in config file;
        //       in case of absence of config file, working dir should be
        //       tried for presence of file named `bootstrap.txt`, only if it
        //       is missing fall back on hardcoded nodes
    */
    bootstrap::bootstrap_hardcoded(&mut tox);

    println!("\nMy ID: {}", tox.get_address());
    println!("My name: {:?}", tox.get_name());


    loop {
        // first process current queue of custom spam messages
        for rec in &bot.receivers {
            if let Ok(fnum) = rec.try_recv() {
                spam_message(&mut tox, fnum, &mut bot.random);
            }
        }

        // now process whatever pending events there are
        for ev in tox.iter() {
            match ev {
                FriendRequest(fpk, msg) => {
                    on_friend_request(&mut tox, fpk, msg);
                },

                FriendMessage(fnum, _msgkind, msg) => {
                    on_friend_message(&mut tox, fnum, msg, &mut bot);
                },

                _ => log(&format!("Event: {:?}", ev)),
            }
        }

        /*
            Mass-spam functionality.
        */
        for fnum in &bot.spam {
            spam_message(&mut tox, *fnum, &mut bot.random);
            // ↓ be ready to send message to the next friend on the list
            tox.tick();
        }

        // "last" thing in loop to do is to add new threads when requested
        if let Some(spam) = bot.cust_spam.pop() {
            let (fnum, spam) = spam;
            bot.receivers.push(hidden_iter_crouching_spam(fnum, spam));
        }


        /*
            Write save data every 64s.

            After a write, be it successful or not, set clock again to tick,
            for the next time when it'll need to be saved.
            TODO: save data every $relevant_event, rather than on timer.
        */
        let cur_time = UTC::now().timestamp();
        if bot.last_save + 64 < cur_time {
            match for_files::write_save("bot.tox", tox.save()) {
                Ok(_) => println!("{}: File saved.", UTC::now()),
                Err(e) => println!("\n{}: Failed to save file: {}",
                                UTC::now(), e),
            }
            bot.last_save = cur_time;
        }

    // needed to run
    tox.wait();
    }
}
